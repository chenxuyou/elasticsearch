package com.cxy.elasticsearch.controller;

import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.IndicesClient;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
public class EsController {
    @Autowired
    RestHighLevelClient client;

    @Autowired
    RestClient restClient;
    @RequestMapping(value = "/createIndex" ,method = RequestMethod.POST)
    public String createIndex(){
        //创建索引请求对象
        CreateIndexRequest createIndexRequest = new CreateIndexRequest("chenxuyou2");
        // 设置参数
        createIndexRequest.settings(Settings.builder().put("number_of_shards","1").put("number_of_replicas","0"));
        //指定映射
        createIndexRequest.mapping("doc"," {\n" +
                " \t\"properties\": {\n" +
                "            \"studymodel\":{\n" +
                "             \"type\":\"keyword\"\n" +
                "           },\n" +
                "            \"name\":{\n" +
                "             \"type\":\"keyword\"\n" +
                "           },\n" +
                "           \"description\": {\n" +
                "              \"type\": \"text\",\n" +
                "              \"analyzer\":\"ik_max_word\",\n" +
                "              \"search_analyzer\":\"ik_smart\"\n" +
                "           },\n" +
                "           \"pic\":{\n" +
                "             \"type\":\"text\",\n" +
                "             \"index\":false\n" +
                "           }\n" +
                " \t}\n" +
                "}", XContentType.JSON);
        //指定索引操作的客户端
        IndicesClient indices = client.indices();
        //执行创建索引库
        CreateIndexResponse createIndexResponse = null;
        try {
            createIndexResponse = indices.create(createIndexRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean acknowledged = createIndexResponse.isAcknowledged();
        //获取返回结果
        System.err.println(acknowledged);
        return "ok";
    }
    @RequestMapping(value = "/deleteIndex",method = RequestMethod.POST)
    public String deleteIndex(){
        DeleteIndexRequest chenxuyou3 = new DeleteIndexRequest("chenxuyou2");
        IndicesClient indices = client.indices();
        AcknowledgedResponse delete =null;
        try {
            delete= indices.delete(chenxuyou3, RequestOptions.DEFAULT);
           // delete = indices.delete(chenxuyou3);
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean acknowledged = delete.isAcknowledged();
        System.err.println(acknowledged);
        return "";
    }
    @RequestMapping(value = "/addDoc",method = RequestMethod.POST)
    public String addDoc(){
//文档内容
        //准备json数据
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("name", "spring cloud实战");
        jsonMap.put("description", "本课程主要从四个章节进行讲解： 1.微服务架构入门 2.spring cloud 基础入门 3.实战Spring Boot 4.注册中心eureka。");
        jsonMap.put("studymodel", "201001");
        SimpleDateFormat dateFormat =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        jsonMap.put("timestamp", dateFormat.format(new Date()));
        jsonMap.put("price", 5.6f);

        //创建索引创建对象
        //带有type的方法已经废弃
        IndexRequest indexRequest = new IndexRequest("chenxuyou3","doc");
       // IndexRequest indexRequest = new IndexRequest("chenxuyou3");
        //文档内容
        indexRequest.source(jsonMap);
        //通过client进行http的请求
        IndexResponse indexResponse = null;
        try {
            indexResponse = client.index(indexRequest,RequestOptions.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        DocWriteResponse.Result result = indexResponse.getResult();
        System.err.println(result);
        return "ok";
    }
    @RequestMapping(value = "/selectDoc",method = RequestMethod.GET)
    public String selectDoc(){
        //查询请求对象
       // GetRequest getRequest = new GetRequest("chenxuyou2","doc","8tyV-m4B7rvW_ZY4LvVU");
        GetRequest getRequest = new GetRequest("chenxuyou3","doc","ekBpCG8BAmmLmqjtgRwU");
        GetResponse getResponse = null;
        try {
            getResponse = client.get(getRequest,RequestOptions.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //得到文档的内容
        Map<String, Object> sourceAsMap = getResponse.getSourceAsMap();
        System.out.println(sourceAsMap);
        return "ok" ;
    }


}
