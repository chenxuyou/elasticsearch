package com.cxy.elasticsearch.controller;

import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.snapshots.SnapshotShardsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.transform.Source;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class EsindexController {
    @Autowired
    RestHighLevelClient client;

    @Autowired
    RestClient restClient;

    @RequestMapping(value = "/updatedoc",method = RequestMethod.GET)
    public String updatedoc(){
        UpdateRequest updateRequest = new UpdateRequest("chenxuyou3","doc","ekBpCG8BAmmLmqjtgRwU");
        HashMap<String, String> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put("name","Ealsticseach学习实战");
        updateRequest.doc(stringStringHashMap);
        UpdateResponse update =null;
        try {
            update= client.update(updateRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        RestStatus status = update.status();
        System.err.println(status);

        return "ok" ;
    }
    @RequestMapping(value = "/deletedoc",method = RequestMethod.GET)
    public String deletedoc(){
       String id ="e0CECG8BAmmLmqjt6hwW";
       DeleteRequest deleteRequest = new DeleteRequest("chenxuyou3", "doc", id);
        DeleteResponse delete = null;
        try {
            delete = client.delete(deleteRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        RestStatus status = delete.status();
        System.err.println(status);

        return "ok" ;
    }

}
