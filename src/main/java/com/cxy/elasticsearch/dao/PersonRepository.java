package com.cxy.elasticsearch.dao;


import com.cxy.elasticsearch.domain.Person;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface PersonRepository extends ElasticsearchRepository<Person, Integer> {

}